import torch, numpy as np
import torch.nn as nn
import torch.nn.functional as F

from torchvision.models import resnet50
from torchvision.models._utils import IntermediateLayerGetter

from data.config import cfg_re50

backbone = resnet50(pretrained=True)
origin_body = IntermediateLayerGetter(
    backbone, return_layers=cfg_re50['return_layers']).cuda() #{'layer2': 1, 'layer3': 2, 'layer4': 3}


img = np.full((480, 800, 3), fill_value=127, dtype=np.float32)
img -= (104, 117, 123)
img = img.transpose(2, 0, 1)
img = torch.from_numpy(img).unsqueeze(0).cuda()

origin_output = origin_body(img)

print(origin_output)