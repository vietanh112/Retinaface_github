import torch
from torch import nn, jit
from typing import Union


def convert2ts(model:nn.Module, size:tuple[int, int]) -> jit.ScriptModule:
    model = jit.trace(model, torch.rand((1, 3, *size)))
    model = jit.script(model)
    model = jit.freeze(model)
    return model


def convert2trt(model:nn.Module, size:tuple[int, int]) -> Union[jit.ScriptModule, nn.Module]:
    try:
        import tensorrt, torch_tensorrt
        torch_tensorrt.logging.set_reportable_log_level(torch_tensorrt.logging.Level.Error)
        demo = torch.full(size=(1, 3, *size), fill_value=0.7, dtype=torch.float32, device='cuda')
        model.cuda()
        model = torch.jit.trace(model, demo)
        model = torch.jit.script(model)
        model = torch.jit.freeze(model)
        inputs = [
            torch_tensorrt.Input((1, 3, *size), dtype = torch.float32,
                                 format=torch_tensorrt.TensorFormat.contiguous)
        ]
        with torch_tensorrt.logging.errors():
            model = torch_tensorrt.ts.compile(model, inputs=inputs, enabled_precisions={torch.float32})

    except Exception as ex:
        print(ex)
        
    finally:
        return model
